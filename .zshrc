# Created by newuser for 5.0.2

# ------------------------------
# General Settings
# ------------------------------
export EDITOR=vim          # エディタをvimに設定
export LANG=ja_JP.UTF-8    # 文字コードをUTF-8に設定
export KCODE=u             # KCODEにUTF-8を設定
export AUTOFEATURE=true    # autotestでfeatureを動かす

bindkey -e     # キーバインドをviモードに設定

setopt no_beep             # ビープ音を鳴らさないようにする
setopt auto_cd             # ディレクトリ名の入力のみで移動する
setopt auto_pushd          # cd時にディレクトリスタックにpushdする
setopt correct             # コマンドのスペルを訂正する
setopt magic_equal_subst   # =以降も補完する(--prefix=/usrなど)
setopt prompt_subst        # プロンプト定義内で変数置換やコマンド置換を扱う
setopt notify              # バックグラウンドジョブの状態変化を即時報告する
setopt equals              # =commandを`which command`と同じ処理にする

### Complement ###
autoload -U compinit
compinit                                           # 補完機能を有効にする
setopt auto_list                                   # 補完候補を一覧で表示する(d)
setopt auto_menu                                   # 補完キー連打で補完候補を順に表示する(d)
setopt list_packed                                 # 補完候補をできるだけ詰めて表示する
setopt list_types                                  # 補完候補にファイルの種類も表示する
bindkey "^[[Z" reverse-menu-complete               # Shift-Tabで補完候補を逆順する("\e[Z"でも動作する)
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}' # 補完時に大文字小文字を区別しない
# 補完関数の表示を強化する
zstyle ':completion:*' verbose yes
zstyle ':completion:*' completer _expand _complete _match _prefix _approximate _list _history
zstyle ':completion:*:default' menu select=2
zstyle ':completion:*:messages' format '%F{YELLOW}%d'$DEFAULT
zstyle ':completion:*:warnings' format '%F{RED}No matches for:''%F{YELLOW} %d'$DEFAULT
zstyle ':completion:*:descriptions' format '%F{YELLOW}completing %B%d%b'$DEFAULT
zstyle ':completion:*:options' description 'yes'
zstyle ':completion:*:descriptions' format '%F{yellow}Completing %B%d%b%f'$DEFAULT
# マッチ種別を別々に表示
zstyle ':completion:*' group-name ''
# セパレータを設定する
zstyle ':completion:*' list-separator '-->'
zstyle ':completion:*:manuals' separate-sections true



### Glob ###
setopt extended_glob       # グロブ機能を拡張する
unsetopt caseglob          # ファイルグロブで大文字小文字を区別しない
zle -la history-incremental-pattern-search-backward && bindkey "^r" history-incremental-pattern-search-backward
zle -la history-incremental-pattern-search-forward  && bindkey "^s" history-incremental-pattern-search-forward

### History ###
HISTFILE=~/.zsh_history     # ヒストリを保存するファイル
HISTSIZE=10000              # メモリに保存されるヒストリの件数
SAVEHIST=10000              # 保存されるヒストリの件数
setopt bang_hist            # !を使ったヒストリ展開を行う(d)
setopt extended_history     # ヒストリに実行時間も保存する
setopt hist_ignore_dups     # 前と同じコマンドはヒストリに追加しない
setopt share_history        # 他のシェルのヒストリをリアルタイムで共有する
setopt hist_reduce_blanks   # 余分なスペースを削除してヒストリに保存する



# ------------------------------
# Look And Feel Settings
# ------------------------------
autoload -Uz is-at-least
autoload -Uz add-zsh-hook

## visual settings
# title
case "${TERM}" in
  kterm*|xterm*)
    set_title() {
      echo -ne "\033]0;${USER}@${HOST%%.*}\007"
    }
    add-zsh-hook precmd set_title
    ;;
esac

# colors
local DEFAULT='%{[m%}'
local RED='%{[1;31m%}'
local GREEN='%{[1;32m%}'
local YELLOW='%{[1;33m%}'
local BLUE='%{[1;34m%}'
local MAGENTA='%{[1;35m%}'
local CYAN='%{[1;36m%}'
local WHITE='%{[1;37m%}'
case "${OSTYPE}" in
  darwin*)
    export CLICOLOR=1
    export LSCOLORS=gxfxheDxbxegefbgbegcgd
    ;;
esac
export LS_COLORS='rs=0:di=36;40:ln=35;40:mh=00:pi=40;33:so=37;44:do=01;35:bd=34;46;01:cd=34;45;01:or=40;31;01:su=31;46:sg=31;44:ca=30;41:tw=36;42:ow=36;43:st=37;44:ex=31;40:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:'
export ZLS_COLORS=$LS_COLORS

# prompt
setopt prompt_subst
setopt prompt_percent
setopt transient_rprompt
unsetopt promptcr
case "${TERM}" in
  dumb)
    PROMPT='%m:%c %n%(!.#.$) '
    ;;
  *)
    autoload -Uz vcs_info
    zstyle ':vcs_info:*' enable git hg
    if is-at-least 4.3.10; then
      zstyle ':vcs_info:*' check-for-changes true
    fi
    zstyle ':vcs_info:*' stagedstr '%{${fg[yellow]}%}!%{${reset_color}%}'
    zstyle ':vcs_info:*' unstagedstr '%{${fg[red]}%}+%{${reset_color}%}'
    zstyle ':vcs_info:*' formats '%{${fg[green]}%}(%s)-[%c%u%{${fg[green]}%}%r:%b/%S]'
    zstyle ':vcs_info:*' actionformats '%{${fg[red]}%}(%s)-[%r:%b/%S|%a]'
    autoload -Uz colors && colors
    local prompt_main_color='%{${fg[green]}%}'
    local prompt_sub_color='%{${fg[yellow]}%}'
    local prompt_fail_color='%{${fg[red]}%}'
    local prompt_ssh_color='%{${fg[cyan]}%}'
    PROMPT="%(?.${prompt_main_color}.${prompt_fail_color})%n@%m ${prompt_sub_color}%c ${prompt_main_color}%(!.#.$) %{${reset_color}%}"
    PROMPT2="${prompt_main_color}%_ > %{${reset_color}%}"
    SPROMPT="${prompt_fail_color}correct: ${prompt_sub_color}%R ${prompt_fail_color}-> ${prompt_main_color}%r ${prompt_fail_color}[nyae]? %{${reset_color}%}"
    init_vcs_info(){
      LANG=en_US.UTF-8 vcs_info
      if [ -z ${vcs_info_msg_0_} ]; then
        RPROMPT="${prompt_main_color}[%~]%{${reset_color}%}"
      else
        RPROMPT="${vcs_info_msg_0_}%{${reset_color}%}"
      fi
    }
    add-zsh-hook precmd init_vcs_info
    ;;
esac
if [ -n "${SSH_CLIENT}${SSH_CONNECTION}" ]; then
  PROMPT=$CYAN"[ssh] "$PROMPT$DEFAULT
fi




### Ls Color ###
# 色の設定
# export LSCOLORS=Exfxcxdxbxegedabagacad
# 補完時の色の設定
# export LS_COLORS='di=01;34:ln=01;35:so=01;32:ex=01;31:bd=46;34:cd=43;34:su=41;30:sg=46;30:tw=42;30:ow=43;30'
# ZLS_COLORSとは？
# export ZLS_COLORS=$LS_COLORS
# lsコマンド時、自動で色がつく(ls -Gのようなもの？)
export CLICOLOR=true
# 補完候補に色を付ける
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}






# ------------------------------
# Other Settings
# ------------------------------

### Aliases ###
alias v='vim'
alias ls='ls --color'
alias l='ls --color'
alias reshell='exec $SHELL -l'
#alias vim='nvim'


#. /home/kei/Git/torch/install/bin/torch-activate
