if &compatible
	set nocompatible
endif

" reset augroup
augroup MyAutoCmd
  autocmd!
augroup END

" dein settings {{{
" dein自体の自動インストール
let s:cache_home = empty($XDG_CACHE_HOME) ? expand('~/.cache') : $XDG_CACHE_HOME
let s:dein_dir = s:cache_home . '/dein'
let s:dein_repo_dir = s:dein_dir . '/repos/github.com/Shougo/dein.vim'
if !isdirectory(s:dein_repo_dir)
  call system('git clone https://github.com/Shougo/dein.vim ' . shellescape(s:dein_repo_dir))
endif
let &runtimepath = s:dein_repo_dir .",". &runtimepath
" プラグイン読み込み＆キャッシュ作成
let s:toml_file = fnamemodify(expand('<sfile>'), ':h').'/dein.toml'
if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir)
  call dein#load_toml(s:toml_file)
  call dein#end()
  call dein#save_state()
endif
if dein#check_install()
	call dein#install()
endif
"}}}

filetype plugin indent on
syntax enable



set termguicolors
set number
set clipboard=unnamed
set guifont=Ubuntu\ Mono\ 13	" font
set tabstop=4           " tab幅
set shiftwidth=4        " シフト幅
set wildmenu            " コマンドの補完
set wildchar=<tab>      " tabで補完
set wildmode=list:full  " リスト表示の最長マッチ
set encoding=utf8       " エンコード
set noswapfile          " swapは作らない
set nobackup            " backup春くらない 
set foldmethod=marker   " マーカ部分は折りたたむ
set cursorline          " 現在の場所
set scrolloff=3         " 端
set guioptions+=a       "
set wrap                " 次の行に表示
set shiftround          " shiftwidthに丸め込み
set commentstring=////%s        " マーカー
set whichwrap=b,s,h,l,<,>,[,]   " 行頭・行末で止まらない
set backspace=indent,eol,start  " バックスペースで消す
" set completeopt-=preview

" c/c++
set tags=tags
set autoindent
set smartindent


" なぜかこっちに書かなと動かない
" いつかなんとかする
" deoplete.nvim
let g:deoplete#enable_at_startup = 1
let g:deoplete#auto_complete_delay = 0
let g:deoplete#auto_complete_start_length = 1
let g:deoplete#enable_camel_case = 0
let g:deoplete#enable_ignore_case = 0
let g:deoplete#enable_refresh_always = 0
let g:deoplete#enable_smart_case = 1
let g:deoplete#file#enable_buffer_path = 1
let g:deoplete#max_list = 10000
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ deoplete#manual_complete()
function! s:check_back_space() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1]  =~ '\s'
endfunction

autocmd CompleteDone * pclose!



