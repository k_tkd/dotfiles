scriptencoding utf-8

if has('vim_starting')
	set nocompatible
endif

" C++ の標準ライブラリへのパス
" $VIM_CPP_INCLUDE_DIR とは別に設定しておく
" default : ""
let $VIM_CPP_STDLIB = "/usr/include/c++/5/*,/usr/include/*"


" set {{{
set clipboard=unnamed
set guifont=Ubuntu\ Mono\ 13	" font
set nocompatible        " 
set tabstop=4           " tab幅
set shiftwidth=4        " シフト幅
set wildmenu            " コマンドの補完
set wildchar=<tab>      " tabで補完
set wildmode=list:full  " リスト表示の最長マッチ
set complete+=k         " 辞書ファイル追加
set encoding=utf8       " エンコード
set number              " 行番号
set noswapfile          " swapは作らない
set nobackup            " backup春くらない 
set foldmethod=marker   " マーカ部分は折りたたむ
set hlsearch            " 探索結果をハイライト
set incsearch           " 書いている間にハイライト
set cursorline          " 現在の場所
set scrolloff=5         " 端
set vb t_vb=            " ヒープ音消す
set laststatus=2        " 常にステータスラインを表示
set ruler               " カーソルが何行目
set novisualbell        " 
set mouse=a             " ターミナルでマウス使用 
set guioptions+=a       "
set showmatch           " 括弧の一致箇所
set wrap                " 次の行に表示
set shiftround          " shiftwidthに丸め込み
set infercase           
set commentstring=////%s        " マーカー
set whichwrap=b,s,h,l,<,>,[,]   " 行頭・行末で止まらない
set backspace=indent,eol,start  " バックスペースで消す
"}}}

" map {{{
nnoremap j gj
nnoremap k gk
nnoremap <Down> gj
nnoremap <Up> gk

nnoremap tc :tabnew<CR>
nnoremap tn :tabnext<CR>
nnoremap tp :tabprevious<CR>

nnoremap <C-]> g<C-]>

"}}}

"shell のパスを設定
if has("gui_win32")
	set shell=/bin/zsh
endif

" neobundle.vim の設定 {{{

" neobundle.vim がインストールするプラグインへのパス
" neobundle.vim もこのディレクトリにインストールが行われる
" default : ""
let $VIM_NEOBUNDLE_PLUGIN_DIR = "~/.vim/bundle"

" C++ のインクルードディレクトリ
" 複数の場合は , 区切りで設定
" default : ""
let $VIM_CPP_INCLUDE_DIR = "/usr/include/boost/*"

" プラグインのインストールディレクトリ
let s:neobundle_plugins_dir = expand(exists("$VIM_NEOBUNDLE_PLUGIN_DIR") ? $VIM_NEOBUNDLE_PLUGIN_DIR : '~/.vim/bundle')

" インクルードディレクトリ
let s:cpp_include_dirs = expand(exists("$VIM_CPP_INCLUDE_DIR") ? $VIM_CPP_INCLUDE_DIR : '')

" }}}

" プラグインの自動読み込み {{{

" プラグインの読み込み
if !executable("git")
	echo "Please install git."
	finish
endif

if !isdirectory(s:neobundle_plugins_dir . "/neobundle.vim")
	echo "Please install neobundle.vim."
	function! s:install_neobundle()
		if input("Install neobundle.vim? [Y/N] : ") =="Y"
			if !isdirectory(s:neobundle_plugins_dir)
				call mkdir(s:neobundle_plugins_dir, "p")
			endif

			execute "!git clone git://github.com/Shougo/neobundle.vim "
			\ . s:neobundle_plugins_dir . "/neobundle.vim"
			echo "neobundle installed. Please restart vim."
		else
			echo "Canceled."
		endif
	endfunction
	augroup install-neobundle
		autocmd!
		autocmd VimEnter * call s:install_neobundle()
	augroup END
	finish
endif

" }}}


" neobundle.vim でプラグインを読み込む
" https://github.com/Shougo/neobundle.vim
if has('vim_starting')
	execute "set runtimepath+=" . s:neobundle_plugins_dir . "/neobundle.vim"
endif
call neobundle#begin(s:neobundle_plugins_dir)
" neobundle 自身を neobundle で管理
NeoBundleFetch "Shougo/neobundle.vim"
" コメントアウト
NeoBundle 'tomtom/tcomment_vim'
" スニペット
NeoBundle "Shougo/neosnippet.vim"
" NeoBundle "Shougo/neosnippet-snippets"
" C++ のシンタックス
NeoBundle "vim-jp/cpp-vim"

" コード補完
" どちらか一方です
NeoBundle "osyo-manga/vim-marching"
" NeoBundle "justmao945/vim-clang"
" NeoBundle "Shougo/neoinclude.vim"

" headerを開く
NeoBundle 'kana/vim-altr'	
" colorscheme
NeoBundle "sjl/badwolf"
NeoBundle "tomasr/molokai"
NeoBundle "w0ng/vim-hybrid"

" 検索系
NeoBundle "nelstrom/vim-visual-star-search"

" NeoBundle "davidhalter/jedi-vim", {
" 			\ "autoload": {"filetype":["python","python3"] }}

" 汎用的なコード補完プラグイン
" +lua な環境であれば neocomplete.vim を利用する
if has("lua")
	NeoBundle "Shougo/neocomplete.vim"
else
	NeoBundle "Shougo/neocomplcache"
endif

" vimproc.vim
" vimproc.vim を使用する場合は自前でビルドする必要があり
" kaoriya 版 vim では vimproc.vim が同梱されているので必要がないです
if !has("kaoriya")
	NeoBundle 'Shougo/vimproc.vim', {
	\ 'build' : {
	\     'windows' : 'make -f make_mingw32.mak',
	\     'cygwin' : 'make -f make_cygwin.mak',
	\     'mac' : 'make -f make_mac.mak',
	\     'unix' : 'make -f make_unix.mak',
	\    },
	\ }
endif

call neobundle#end()

" filetypeごとのplugin indentをon
filetype plugin indent on
" 未インストールのチェック
NeoBundleCheck





" neocomplet.vim {{{
" 補完を有効にする
let g:neocomplete#enable_at_startup = 1
" 補完に時間がかかってもスキップしない
let g:neocomplete#skip_auto_completion_time = ""
if !exists('g:neocomplete#force_omni_input_patterns')
	let g:neocomplete#force_omni_input_patterns = {}
endif
" let g:neocomplete#force_omni_input_patterns.cpp =
" 			\ '[^.[:digit:] *\t]\%(\.\|->\)\w*\|\h\w*::\w*'
let g:neocomplete#force_overwrite_completefunc = 1
let g:neocomplete#force_omni_input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
let g:neocomplete#force_omni_input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'
"}}}


" vim altr f2(head jump) {{{
nmap <F2> <Plug>(altr-forward)
" }}}

" neosnippet.vim {{{
" スニペットを展開するキーマッピング
" <Tab> で選択されているスニペットの展開を行う
" 選択されている候補がスニペットであれば展開し、
" それ以外であれば次の候補を選択する
" また、既にスニペットが展開されている場合は次のマークへと移動する
imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
            \ "\<Plug>(neosnippet_expand_or_jump)"
            \: pumvisible() ? "\<C-n>" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
            \ "\<Plug>(neosnippet_expand_or_jump)"
            \: "\<TAB>"
" let g:neosnippet#snippets_directory = "~/.neosnippet"

" 現在の filetype のスニペットを編集する為のキーマッピング
" こうしておくことでサッと編集や追加などを行うことができる
" 以下の設定では新しいタブでスニペットファイルを開く
" nnoremap <Space>ns :execute "tabnew\|:NeoSnippetEdit ".&filetype<CR>

" }}}

" marching.vim c++ 補完 {{{
let g:marching_clang_command = "/usr/bin/clang++"	

if !empty(g:marching_clang_command) && executable(g:marching_clang_command)
    " 非同期ではなくて同期処理で補完する
    " let g:marching_backend = "sync_clang_command"
else
    " clang コマンドが実行できなければ wandbox を使用する
    let g:marching_backend = "wandbox"
    let g:marching_clang_command = ""
endif

" オプションの設定
" これは clang のコマンドに渡される
let g:marching#clang_command#options = {
\	"cpp" : "-std=c++11"
\}

let g:marching_include_paths = filter(
\   split(glob('/usr/include/c++/*'), '\n') +
\   split(glob('/usr/include/*/c++/*'), '\n') +
\   split(glob('/usr/include/*/'), '\n') +
\   split(glob('/home/kei/Git/benzene-asel/src/*/'), '\n'),
\   'isdirectory(v:val)')

if neobundle#is_sourced("neocomplete.vim")
    " neocomplete.vim と併用して使用する場合
    let g:marching_enable_neocomplete = 1
endif

" }}}

" 'justmao945/vim-clang' {{{
" disable auto completion for vim-clanG
" let g:clang_auto = 0
" let g:clang_complete_auto = 0
" let g:clang_auto_select = 0
" let g:clang_use_library = 1
"
" " default 'longest' can not work with neocomplete
" let g:clang_c_completeopt   = 'menuone'
" let g:clang_cpp_completeopt = 'menuone'
"
" if executable('clang-3.8')
" 	let g:clang_exec = 'clang-3.8'
" else
" 	let g:clang_exec = 'clang'
" endif
"
" if executable('clang-format-3.8')
" 	let g:clang_format_exec = 'clang-format-3.8'
" else
" 	let g:clang_exec = 'clang-format'
" endif
"
" let g:clang_c_options = '-std=c11'
" let g:clang_cpp_options = '-std=c++11 -stdlib=libc++'

" }}}

"   .cpp の固有の設定  {{{

augroup vimrc-cpp
	autocmd!
	" filetype=cpp が設定された場合に関数を呼ぶ
	autocmd FileType cpp call s:cpp()
augroup END


function! s:cpp()
	" インクルードパスを設定する
	" gf などでヘッダーファイルを開きたい場合に影響する
	let &l:path = join(filter(split($VIM_CPP_STDLIB . "," . $VIM_CPP_INCLUDE_DIR, '[,;]'), 'isdirectory(v:val)'), ',')

	" 括弧を入力した時にカーソルが移動しないように設定
	set matchtime=0
	" CursorHold の更新間隔
	set updatetime=100
	let c_comment_strings=1
	let c_no_curly_error=1

	" 括弧を構成する設定に <> を追加する
	" template<> を多用するのであれば
	setlocal matchpairs+=<:>

    " インデント
    setlocal autoindent
    setlocal smartindent
    set cindent
	set expandtab
    set autochdir   "現在のディレクトリに移動
	" タグファイル 上層のtagsファイルを読み込む
	" プロジェクトファイルのrootにタグファイルがあることを想定している
	set tags=tags;

    " chacheをインサートから抜けた時に削除
    autocmd! * <buffer>
    autocmd InsertLeave <buffer> MarchingBufferClearCache

	if exists("*CppVimrcOnFileType_cpp")
		call CppVimrcOnFileType_cpp()
	endif
endfunction

" }}}

"   .py の固有の設定 {{{

" augroup vimrc-python
" 	autocmd!
" 	" filetype=cpp が設定された場合に関数を呼ぶ
" 	autocmd FileType python call s:py()
" augroup END
"
" function! s:py()
" 	" 次の行のインデントを合わせる
" 	set autoindent
" 	" 次行のインデントを自動で行ってくれる
" 	set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
" 	set tabstop=8 expandtab shiftwidth=4 softtabstop=4
"
"
" 	# 
" 	let g:jedi#auto_initialization = 1
" 	let g:jedi#auto_vim_configuration = 1
"
" 	nnoremap [jedi] <Nop>
" 	xnoremap [jedi] <Nop>
" 	nmap <Leader>j [jedi]
" 	xmap <Leader>j [jedi]
"
" 	"補完キーの設定この場合はCtrl+Space
" 	let g:jedi#completions_command = "<C-Space>"	
" 	"変数の宣言場所へジャンプ（Ctrl + g)
" 	let g:jedi#goto_assignments_command = "<C-g>"   
" 	" クラス、関数定義にジャンプ（Gtrl + d）
" 	let g:jedi#goto_definitions_command = "<C-d>"   
" 	" Pydocを表示（Ctrl + k）
" 	let g:jedi#documentation_command = "<C-k>"      
" 	let g:jedi#rename_command = "[jedi]r"
" 	let g:jedi#usages_command = "[jedi]n"
" 	let g:jedi#popup_select_first = 0
" 	let g:jedi#popup_on_dot = 0
"
" 	setlocal completeopt-=preview
"
" 	" for w/ neocomplete
" 	if ! empty(neobundle#get("neocomplete.vim"))
" 		setlocal omnifunc=jedi#completions
" 		let g:jedi#completions_enabled = 0
" 		let g:jedi#auto_vim_configuration = 0
" 		let g:neocomplete#force_omni_input_patterns.python =
" 			\ '\%([^. \t]\.\|^\s*@\|^\s*from\s.\+import \|^\s*from \|^\s*import \)\w*'
" 	endif
"
" endfunction



" }}}

" .tex の設定 {{{
augroup vimrc-tex
	autocmd!
	autocmd FileType tex call s:tex()
augroup END

" これは共通設定
set spelllang=en,cjk
function! s:tex()
	redir! => syntax
	silent syntax
	redir END

	set spell

	if syntax =~? '/<comment\>'
		syntax spell default
		syntax match SpellMaybeCode /\<\h\l*[_A-Z]\h\{-}\>/ contains=@NoSpell transparent containedin=Comment contained
	else
		syntax spell toplevel
		syntax match SpellMaybeCode /\<\h\l*[_A-Z]\h\{-}\>/ contains=@NoSpell transparent
	endif

	syntax cluster Spell add=SpellNotAscii,SpellMaybeCode


	" 次の行のインデントを合わせる
	set autoindent
	" 次行のインデントを自動で行ってくれる
	set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
	set tabstop=8 expandtab shiftwidth=4 softtabstop=4
endfunction

" }}}

"   色設定 これまだ良くわかんない {{{
" ---------------------------------------------------
" ターミナルタイプによるカラー設定
if &term =~ "xterm-256color" || "screen-256color"
  " 256色
  set t_Co=256
  set t_Sf=[3%dm
  set t_Sb=[4%dm
elseif &term =~ "xterm-debian" || &term =~ "xterm-xfree86"
  set t_Co=16
  set t_Sf=[3%dm
  set t_Sb=[4%dm
elseif &term =~ "xterm-color"
  set t_Co=8
  set t_Sf=[3%dm
  set t_Sb=[4%dm
endif

syntax on                   " シンタックスハイライト
set background=dark
" colorscheme badwolf
" colorscheme molokai
colorscheme hybrid
highligh Normal ctermbg=none    " 半透明へ
" }}} 

