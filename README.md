# dotfiles
2015/7/8

### install neovim 2018/10/27
# reference: https://github.com/neovim/neovim/wiki/Installing-Neovim
# ubuntu 18.04
$ sudo apt install neovim
$ sudo apt install python-dev python-pip python3-dev python3-pip
# set config file path
export XDG_CONFIG_HOME=$HOME/.config
$ pip3 install neovim --upgrade


