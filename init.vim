" $ sudo apt install neovim
" $ sudo apt install python-dev python-pip python3-dev python3-pip
" $ echo 'export XDG_CONFIG_HOME=$HOME/.config' >> ~/.zshenv
" $ pip3 install neovim --upgrade

if &compatible
  set nocompatible
endif

if (!isdirectory(expand("$HOME/.cache/dein/repos/github.com/Shougo/dein.vim")))
    call system(expand("mkdir -p $HOME/.cache/dein/repos/github.com"))
    call system(expand("git clone https://github.com/Shougo/dein.vim $HOME/.cache/dein/repos/github.com/Shougo/dein.vim"))
  endif
set runtimepath+=~/.cache/dein/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.cache/dein')
  call dein#begin('~/.cache/dein')
  call dein#add('Shougo/deoplete.nvim')
  " :UpdateRemotePluginsで治る
  if !has('nvim')
    call dein#add('roxma/nvim-yarp')
    call dein#add('roxma/vim-hug-neovim-rpc')
  endif
  call dein#load_toml('~/.config/nvim/dein.toml', {'lazy': 0})
  call dein#load_toml('~/.config/nvim/dein.toml', {'lazy': 1})
  call dein#end()
  call dein#save_state()
endif

if dein#check_install()
	call dein#install()
endif

filetype plugin indent on
syntax enable

scriptencoding utf-8

set clipboard=unnamedplus
set guifont=Ricty\ Diminished\ 22	" font
set tabstop=4           " tab幅
set shiftwidth=4        " シフト幅
set smarttab
set expandtab			" tabをスペースへ
set wildmenu            " コマンドの補完
set wildchar=<tab>      " tabで補完
set wildmode=list:full  " リスト表示の最長マッチ
set complete+=k         " 辞書ファイル追加
set encoding=utf8       " エンコード
set number              " 行番号
set noswapfile          " swapは作らない
set nobackup            " backup春くらない 
set foldmethod=marker   " マーカ部分は折りたたむ
set hlsearch            " 探索結果をハイライト
set incsearch           " 書いている間にハイライト
set cursorline          " 現在の場所
set scrolloff=5         " 端
set vb t_vb=            " ヒープ音消す
set ruler               " カーソルが何行目
set novisualbell        " 
set mouse=a             " ターミナルでマウス使用 
set guioptions+=a       "
set showmatch           " 括弧の一致箇所
set wrap                " 次の行に表示
set shiftround          " shiftwidthに丸め込み
set infercase           
set commentstring=////%s        " マーカー
set whichwrap=b,s,h,l,<,>,[,]   " 行頭・行末で止まらない
set backspace=indent,eol,start  " バックスペースで消す

nnoremap j gj
nnoremap k gk
nnoremap <Down> gj
nnoremap <Up> gk
nnoremap tc :tabnew<CR>
nnoremap tn :tabnext<CR>
nnoremap tp :tabprevious<CR>
nnoremap <C-]> g<C-]>

"ステータスラインにコマンドを表示
set showcmd
"ステータスラインを常に表示
set laststatus=2
"ファイルナンバー表示
set statusline=[%n]
"ホスト名表示
set statusline+=%{matchstr(hostname(),'\\w\\+')}@
"ファイル名表示
set statusline+=%<%F
"変更のチェック表示
set statusline+=%m
"読み込み専用かどうか表示
set statusline+=%r
"ヘルプページなら[HELP]と表示
set statusline+=%h
"プレビューウインドウなら[Prevew]と表示
set statusline+=%w
"ファイルフォーマット表示
set statusline+=[%{&fileformat}]
"文字コード表示
set statusline+=[%{has('multi_byte')&&\&fileencoding!=''?&fileencoding:&encoding}]
"ファイルタイプ表示
set statusline+=%y


" neodein
let g:deoplete#enable_at_startup = 1
let g:deoplete#auto_complete_delay = 0
inoremap <silent><expr> <TAB>
    \ pumvisible() ? "\<C-n>" :
    \ <SID>check_back_space() ? "\<TAB>" :
    \ deoplete#mappings#manual_complete()
function! s:check_back_space() abort 
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction



""""""""""""""""""""""""""""""""""""""""""""""
" c++の設定用
"
function! s:cpp()
    setlocal matchpairs+=<:>
    set completeopt=longest,menuone
    set tags=tags;

endfunction

augroup vimrc-cpp
	autocmd!
	" filetype=cpp が設定された場合に関数を呼ぶ
	autocmd FileType cpp call s:cpp()
augroup END

